#!/bin/bash
#
#  filename: ip-collector.sh
#  invocation: ip-collector.sh <file_name>
#  use: feed this script a log file that includes IPs. this script then counts unique IP entries
#+ in that file then it prints the sorted list of IPs - including Country Code (vis-a-vis the python script).
# # # # #
# Global vars
E_CALLED_HELP="55"
E_BAD_ARGS="65"
E_BAD_LOG="75"
# # # # #
# Funcs
usage() {
    echo -e "\nInvocation: $(basename "$0") <file-name>"
    echo "Use: $(basename $0) is used to parse IP addresses out of the given file"
    echo "and then passes those records into a python script that then pulls GeoIP"
    echo "information (re: Country Code) per each address. This script ends by "
    echo -e "printing a sorted list of IP hits per file, including Country Code value.\n"
}
call_for_help() {
    case "${1}" in
        -h|--help)
        usage
        exit "${E_CALLED_HELP}"
        ;;
    esac
}
does_log_exist() {
    [[ -f "${1}" ]] || { echo -e "File ${1} does not appear to exist.\n"; usage; exit "${E_BAD_LOG}"; }
}
make_tmp_files() {
    IP_COUNT="/tmp/$(date +%s)-ip.count"
    IP_CC="/tmp/$(date +%s)-ip.cc"
    [[ -f "${IP_COUNT}" ]] || touch "${IP_COUNT}"
    [[ -f "${IP_CC}" ]] || touch "${IP_CC}"
}
cc_producer() {
    FETCH_CC="/usr/local/bin/cc-producer.py"
    zgrep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" "${1}" | sort -n | uniq -c | sort -n > "${IP_COUNT}"
    awk '{print $2}' "${IP_COUNT}" | python "${FETCH_CC}" > "${IP_CC}"
    paste "${IP_COUNT}" "${IP_CC}" | awk '{print $1, $3, $4}' | column -t
    unset FETCH_CC IP_COUNT IP_CC
}
cleanup() {
    rm -rf "${IP_COUNT}" "${IP_CC}"
}
# # # # #
# "Main"
# ------
# maximally, user calls one arg: --help or <file>. else, call usage() and exit.
[[ "$#" -eq 1 ]] || { usage; exit "${E_BAD_ARGS}"; }
call_for_help "${1}"
does_log_exist "${1}"
make_tmp_files
cc_producer "${1}"
cleanup