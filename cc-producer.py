#!/usr/bin/python3.6
import sys
import GeoIP
from IPy import IP

def validate_ipv4(s):
    pieces = s.split('.')
    if len(pieces) != 4: return False
    try: return all(0<=int(p)<256 for p in pieces)
    except ValueError: return False

# use stdin if it's full
if not sys.stdin.isatty():
    input_stream = sys.stdin

# otherwise, read the given filename
else:
    try:
        input_filename = sys.argv[1]
    except IndexError:
        message = 'If stdin is not full, we need a filename as first argument.'
        raise IndexError(message)
    else:
        input_stream = open(input_filename, 'rU')

geoip_answer = GeoIP.new(GeoIP.GEOIP_MEMORY_CACHE)
ip_list = [line.strip() for line in input_stream]

for address in ip_list:
    if validate_ipv4(address):
        if geoip_answer.country_code_by_addr(address):
            print(geoip_answer.country_code_by_addr(address) + ": " + address)
        else:
            print('No_CC: ' + address)
    else:
        print('Invalid_IP: ' + address)
